import React from "react";
import { graphql, Link } from "gatsby";
import IndexLayout from "../layouts";
import styled from "styled-components";
import { colors } from "../styles/variables";
import Image from "gatsby-image";

const Container = styled.div`
  display: grid;
  grid-gap: 20px;
  margin: 20px;
  justify-content: center;
`;

const StyledAuthor = styled.div`
  display: grid;
  grid-template-columns: 1fr 4fr;
  align-items: center;
  grid-gap: 10px;
`;

const StyledAuthorBio = styled.div`
  display: grid;
  align-items: center;
  & > * {
    margin: 0;
  }
`;

const StyledImage = styled(Image)`
  border-radius: 50%;
`;

type Post = {
  slug: string;
  title: string;
  createdAt: string;
};

type PostWithDateType = {
  slug: string;
  title: string;
  createdAt: Date;
};

const toDateType = (str: string): Date => new Date(str);
const mapStringToDate = (posts: Post[]): PostWithDateType[] =>
  posts.map((post: Post) => ({ slug: post.slug, title: post.title, createdAt: toDateType(post.createdAt) }));

const sortByDate = (array: PostWithDateType[]): PostWithDateType[] =>
  array.sort((a: PostWithDateType, b: PostWithDateType) => a.createdAt.getTime() - b.createdAt.getTime());

const compose = (...fns: Function[]) => (posts: Post[]) => fns.reduceRight((acc, fn: Function) => fn(acc), posts);

const withSortByDate = compose(sortByDate, mapStringToDate);

const Author: React.FC = ({ data }: any) => (
  <IndexLayout>
    <Container>
      <StyledAuthor>
        {data.contentfulAuthor.avatar ? <StyledImage fixed={data.contentfulAuthor.avatar.fixed} fadeIn={false} /> : <></>}
        <StyledAuthorBio>
          <h1>{data.contentfulAuthor.fullName}</h1>
        </StyledAuthorBio>
      </StyledAuthor>
      <hr></hr>
      {data.contentfulAuthor.post ? (
        withSortByDate(data.contentfulAuthor.post).map((p: Post) => (
          <Link to={`/${p.slug}`} key={p.slug}>
            <h3 style={{ color: colors.lilac }}>{p.title}</h3>
          </Link>
        ))
      ) : (
        <></>
      )}
    </Container>
  </IndexLayout>
);

export default Author;

export const query = graphql`
  query authorQuery($slug: String!) {
    contentfulAuthor(slug: { eq: $slug }) {
      fullName
      slug
      avatar {
        fixed(height: 100, width: 100) {
          ...GatsbyContentfulFixed_withWebp
        }
      }
      post {
        slug
        title
        createdAt
      }
    }
  }
`;
