import React from "react";
import styled, { createGlobalStyle } from "styled-components";
import { fonts, colors } from "../styles/variables";

const StyledLayoutRoot = styled.div`
  padding: 0;
  margin: 0;
`;

interface LayoutRootProps {
  className?: string;
}

const Global = createGlobalStyle`
html {
  margin: 0;
  padding: 0;
  font-size: 14px;
}

body {
  font-family: ${fonts.sansSerif};
  color: ${colors.black};
  background-color: ${colors.white};
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%;
  margin: 0;
  padding: 0 0 40px 0;
}

img {

}

hr {
  width: 100%;
  color: rgba(255,255,255, 0.5)
}

input[type='text'],
input[type='number'],
textarea {
  font-size: 16px;
}

a {
  color: ${colors.brand};
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
}
`;

const LayoutRoot: React.FC<LayoutRootProps> = ({ children, className }) => {
  return (
    <>
      <Global />
      <StyledLayoutRoot className={className}>{children}</StyledLayoutRoot>
    </>
  );
};

export default LayoutRoot;
