import React from "react";
import styled from "styled-components";
import { Link } from "gatsby";

import { heights, colors, dimensions } from "../styles/variables";

const StyledFooter = styled.footer`
  right: 0;
  bottom: 0;
  left: 0;
  height: ${heights.header}px;
  padding: 0 ${dimensions.containerPadding}rem;
  background-color: ${colors.brand};
  text-align: center;
`;

const FooterInner = styled.div`
  display: grid;
  justify-items: start;
  align-items: center;
  height: 100%;
`;

const HomepageLink = styled(Link)`
  color: ${colors.white};
  font-size: 1.5rem;
  font-weight: 600;

  &:hover,
  &:focus {
    text-decoration: none;
  }
`;

interface FooterProps {
  title: string;
}

const Footer: React.FC<FooterProps> = ({ title }) => (
  <StyledFooter>
    <FooterInner>
      <HomepageLink to="/">{title}</HomepageLink>
    </FooterInner>
  </StyledFooter>
);

export default Footer;
