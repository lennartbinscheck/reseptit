import React from "react";
import styled from "styled-components";
import { Link } from "gatsby";
import { HomeCircle } from "@styled-icons/boxicons-solid";
import { transparentize } from "polished";
import { dimensions, colors } from "../styles/variables";

const StyledHeader = styled.header`
  border-radius: 50px;
  padding: ${dimensions.containerPadding}rem;
  background-color: ${transparentize(0.1, colors.white)};
  color: ${colors.accent};
  top: 40%;
  right: 5%;
  position: fixed;
  z-index: 10000;
  display: grid;
  grid-gap: 30px;
`;

const HomepageLink = styled(Link)`
  color: ${colors.accent};

  &:hover,
  &:focus {
    text-decoration: none;
  }
`;

const Header: React.FC = () => (
  <StyledHeader>
    <HomepageLink to="/">
      <HomeCircle size="24" />
    </HomepageLink>
  </StyledHeader>
);

export default Header;
